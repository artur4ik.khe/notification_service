import logging
import time
import requests
import json
import os
from dotenv import load_dotenv
from django.core.management.base import BaseCommand
from notifications_app.models import Mailing, Message, Client
from datetime import datetime

load_dotenv()

auth_token = os.getenv('TOKEN')
headers = {'Authorization': 'Bearer ' + auth_token}
logger = logging.getLogger('main')


def mailing(mail):
    tags = mail.tags.all()
    codes = mail.codes.all()
    target_clients = set()
    clients = Client.objects.all()
    for tag in tags:
        for client in clients:
            if tag in client.tag.all():
                target_clients.add(client)

    for code in codes:
        for client in clients:
            if code == client.code:
                target_clients.add(client)

    for target_client in target_clients:
        message, created = Message.objects.get_or_create(mailing=mail, client=target_client)
        if created:
            logger.info(f"Create message[{message.pk}] status: {message.status}")
        if message.status == 'ND':
            data = {
                "id": message.pk,
                "phone": target_client.phone,
                "text": f"{mail.text}"
            }
            try:
                response = requests.post(f'https://probe.fbrq.cloud/v1/send/{message.pk}', headers=headers,
                                         data=json.dumps(data))

                logger.info(f"message[{message.pk}] sending")
                if response.json()['message'] == 'OK':
                    message.status = 'D'
                    message.save()
                    logger.info(f"message[{message.pk}] delivered. mailing text: {mail.text}, client: {data['phone']}")
            except Exception:
                logger.info(f"message[{message.pk}] don't delivered, trouble server")



class Command(BaseCommand):
    help = 'Displays current time'

    def handle(self, *args, **kwargs):
        logger.info("start mailing")
        while(True):
            time.sleep(5.0)
            mailings = Mailing.objects.all()
            for mail in mailings:
                if (mail.time_start.timestamp() < datetime.now().timestamp() and
                        datetime.now().timestamp() < mail.time_stop.timestamp()):
                    mailing(mail)
