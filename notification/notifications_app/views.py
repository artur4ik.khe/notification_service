import logging
import json
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from datetime import datetime
from . import models


logger = logging.getLogger('common')


class TagSerializator(serializers.ModelSerializer):
    class Meta:
        model = models.Tag
        fields = '__all__'


class TagClients(APIView):

    def get(self, request, pk=None, format=None):
        id = pk
        if id is not None:
            tag = models.Tag.objects.get(pk=id)
            serializer = TagSerializator(tag)
            return Response(serializer.data)
        tags = models.Tag.objects.all()
        serializer = TagSerializator(tags, many=True)
        return Response(serializer.data)

    def post(self, request, pk=None, format=None):
        serializer = TagSerializator(data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info('Created tag')
            return Response({'msg': 'Data created'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        id = pk
        tag = models.Tag.objects.get(pk=id)
        serializer = TagSerializator(tag, data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Tag[{id}] updated')
            return Response({'msg': 'Complete Data Updated'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        id = pk
        tag = models.Tag.objects.get(pk=id)
        serializer = TagSerializator(tag, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Tag[{id}] partial updated')
            return Response({'msg': 'Partial Data Updated'})
        return Response(serializer.errors)

    def delete(self, request, pk, format=None):
        id = pk
        tag = models.Tag.objects.get(pk=id)
        tag.delete()
        logger.info(f'Tag[{id}] deleted')
        return Response({'msg': 'Data Deleted'})


class CodeSerializator(serializers.ModelSerializer):
    class Meta:
        model = models.Code
        fields = '__all__'


class CodeClients(APIView):

    def get(self, request, pk=None, format=None):
        id = pk
        if id is not None:
            code = models.Code.objects.get(pk=id)
            serializer = CodeSerializator(code)
            return Response(serializer.data)
        codes = models.Code.objects.all()
        serializer = CodeSerializator(codes, many=True)
        return Response(serializer.data)

    def post(self, request, pk=None, format=None):
        serializer = CodeSerializator(data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Code created')
            return Response({'msg': 'Data created'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        id = pk
        code = models.Code.objects.get(pk=id)
        serializer = CodeSerializator(code, data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Code[{id}] updated')
            return Response({'msg': 'Complete Data Updated'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        id = pk
        code = models.Code.objects.get(pk=id)
        serializer = CodeSerializator(code, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Tag[{id}] partial updated')
            return Response({'msg': 'Partial Data Updated'})
        return Response(serializer.errors)

    def delete(self, request, pk, format=None):
        id = pk
        code = models.Code.objects.get(pk=id)
        code.delete()
        logger.info(f'Tag[{id}] deleted')
        return Response({'msg': 'Data Deleted'})


class ClientSerializator(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = '__all__'


class Clients(APIView):

    def get(self, request, pk=None, format=None):
        id = pk
        if id is not None:
            client = models.Client.objects.get(pk=id)
            serializer = ClientSerializator(client)
            return Response(serializer.data)
        clients = models.Client.objects.all()
        serializer = ClientSerializator(clients, many=True)
        return Response(serializer.data)

    def post(self, request, pk=None, format=None):
        serializer = ClientSerializator(data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Client created')
            return Response({'msg': 'Data created'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        id = pk
        client = models.Client.objects.get(pk=id)
        serializer = ClientSerializator(client, data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Client[{id}] updated')
            return Response({'msg': 'Complete Data Updated'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        id = pk
        client = models.Client.objects.get(pk=id)
        serializer = ClientSerializator(client, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Client[{id}] partial updated')
            return Response({'msg': 'Partial Data Updated'})
        return Response(serializer.errors)

    def delete(self, request, pk, format=None):
        id = pk
        client = models.Client.objects.get(pk=id)
        client.delete()
        logger.info(f'Client[{id}] deleted')
        return Response({'msg': 'Data Deleted'})


class MessageSerializator(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = '__all__'


class Messages(APIView):

    def get(self, request, pk=None, format=None):
        id = pk
        if id is not None:
            message = models.Message.objects.get(pk=id)
            serializer = MessageSerializator(message)
            return Response(serializer.data)
        messages = models.Message.objects.all()
        serializer = MessageSerializator(messages, many=True)
        return Response(serializer.data)

    def post(self, request, pk=None, format=None):
        serializer = MessageSerializator(data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Message created')
            return Response({'msg': 'Data created'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        id = pk
        message = models.Message.objects.get(pk=id)
        serializer = MessageSerializator(message, data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Message[{id}] updated')
            return Response({'msg': 'Complete Data Updated'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        id = pk
        message = models.Message.objects.get(pk=id)
        serializer = MessageSerializator(message, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Message[{id}] partial updated')
            return Response({'msg': 'Partial Data Updated'})
        return Response(serializer.errors)

    def delete(self, request, pk, format=None):
        id = pk
        message = models.Message.objects.get(pk=id)
        message.delete()
        logger.info(f'Message[{id}] deleted')
        return Response({'msg': 'Data Deleted'})


class MailingSerializator(serializers.ModelSerializer):
    class Meta:
        model = models.Mailing
        fields = '__all__'


class Mailings(APIView):

    def get(self, request, pk=None, format=None):
        id = pk
        if id is not None:
            mailing = models.Mailing.objects.get(pk=id)
            serializer = MailingSerializator(mailing)
            return Response(serializer.data)
        mailings = models.Mailing.objects.all()
        serializer = MailingSerializator(mailings, many=True)
        return Response(serializer.data)

    def post(self, request, pk=None, format=None):
        serializer = MailingSerializator(data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Mailing created')
            return Response({'msg': 'Data created'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        id = pk
        mailing = models.Mailing.objects.get(pk=id)
        serializer = MailingSerializator(mailing, data=request.data)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Mailing[{id}] updated')
            return Response({'msg': 'Complete Data Updated'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        id = pk
        mailing = models.Mailing.objects.get(pk=id)
        serializer = MailingSerializator(mailing, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            logger.info(f'Mailing[{id}] partial updated')
            return Response({'msg': 'Partial Data Updated'})
        return Response(serializer.errors)

    def delete(self, request, pk, format=None):
        id = pk
        mailing = models.Mailing.objects.get(pk=id)
        mailing.delete()
        logger.info(f'Mailing[{id}] deleted')
        return Response({'msg': 'Data Deleted'})


class Stats(APIView):

    def get(self, request, format=None):
        messages = models.Message.objects.all()
        count_delivered_messages = 0
        count_undelivered_messages = 0
        for message in messages:
            if message.status == "D":
                count_delivered_messages += 1
            else:
                count_undelivered_messages += 1
        mailings = models.Mailing.objects.all()
        active_mailing = 0
        not_active_mailing = 0
        for mailing in mailings:
            if (mailing.time_start.timestamp() < datetime.now().timestamp() and
                    datetime.now().timestamp() < mailing.time_stop.timestamp()):
                active_mailing += 1
            else:
                not_active_mailing += 1

        return Response({
            "Delivered msg": f"{count_delivered_messages}",
            "Undelivered msg": f"{count_undelivered_messages}",
            "Active mailing": f"{active_mailing}",
            "Not active mailing": f"{not_active_mailing}"
        })
