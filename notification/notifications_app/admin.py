from django.contrib import admin
from . import models


class TagAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name']
    list_editable = ['name']


class CodeAdmin(admin.ModelAdmin):
    list_display = ['pk', 'value']
    list_editable = ['value']


class MailingAdmin(admin.ModelAdmin):
    list_display = ['pk', 'time_start', 'text', 'time_stop']
    list_editable = ['time_start', 'text', 'time_stop']


class ClientAdmin(admin.ModelAdmin):
    list_display = ['pk', 'phone', 'code', 'time_zone']
    list_editable = ['phone', 'code', 'time_zone']


class MessageAdmin(admin.ModelAdmin):
    list_display = ['pk', 'date_time', 'mailing', 'client', 'status']


admin.site.register(models.Tag, TagAdmin)
admin.site.register(models.Code, CodeAdmin)
admin.site.register(models.Mailing, MailingAdmin)
admin.site.register(models.Client, ClientAdmin)
admin.site.register(models.Message, MessageAdmin)
