from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now
from datetime import datetime

class Tag(models.Model):
    name = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class Code(models.Model):
    value = models.CharField(max_length=3, unique=True)

    def __str__(self):
        return self.value


class Mailing(models.Model):
    time_start = models.DateTimeField()
    text = models.CharField(max_length=255)
    tags = models.ManyToManyField(Tag, blank=True)
    codes = models.ManyToManyField(Code, blank=True)
    time_stop = models.DateTimeField()


class Client(models.Model):
    phone = models.CharField(max_length=11, unique=True)
    code = models.ForeignKey(Code, null=True, on_delete=models.SET_NULL)
    tag = models.ManyToManyField(Tag)
    time_zone = models.IntegerField()

    def __str__(self):
        return self.phone


class Message(models.Model):
    date_time = models.DateTimeField(default=now)
    mailing = models.ForeignKey(Mailing, null=True, on_delete=models.SET_NULL)
    client = models.ForeignKey(Client, null=True, on_delete=models.SET_NULL)

    class StatusOfMessage(models.TextChoices):
        DELIVERED = 'D', _('Message is delivered')
        NOT_DELIVERED = 'ND', _('Message is not delivered')

    status = models.CharField(
            max_length=2,
            choices=StatusOfMessage.choices,
            default=StatusOfMessage.NOT_DELIVERED,
    )

    def is_upperclass(self):
        return self.status in {
            self.StatusOfMessage.DELIVERED,
            self.StatusOfMessage.NOT_DELIVERED,
        }
