Сервис рассылки сообщений

## Описание

Сервис управления рассылками API администрирования и получения статистики.

Что было сделано:
	- реализованы CRUD операции для сущностей: Клиент, Сообщение, Рассылка, Тэг, Код оператора;
	- реализован метод для получения статистик (количество отправленных/неотправленных сообщений, количество активных/неактивных рассылок);
	- создана менеджерская команда для запуска проверки рассылок;
	- добавлено логирование для CRUD операций;
	- отчет по отправленным сообщениям хранится в виде логов в файле information.log;
	- бд поднята при помощи docker-compose.yml.
Логика отправки сообщения:
	После запуска команды, сервис каждые 5 секунды проверяет рассылки. Если рассылка активна, т.е. настоящее время больше старта и меньше времения конца рассылки, то по тегам и кодам мобильных операторов выбираются клиенты и отправляются им сообщения.

Для просмотра API добавлен swagger, доступный по адресу /docs/

Все секретные переменные были вынесены в файл .env

Используемые технологии:
	Python, Django, DRF, Postgres, docker-compose, swagger, poetry, requests.

Инструкция для запуска:
 1. Клонировать репозиторий
 2. Активировать виртуальное окружение
 3. Установить зависимости при помощи команды: poetry intall
 4. Создать файл .env
 5. Запустить команду docker-compose up для поднятия БД
 6. Заходим в директорию notification (cd notification)
 7. Запускаем сервер python manage.py runserver
 8. Запускаем проверку на новые рассылки: python manage.py start_mailing 
 

Файл  .env  заполнить:
	POSTGRES_DB=db_notification
	POSTGRES_USER=admin
	POSTGRES_PASSWORD=admin
	POSTGRES_HOST=127.0.0.1
	POSTGRES_PORT=5432
	SECRET_KEY=django-insecure-+is&4@1em3t^8^qca1w4h90b+pn)!c0t96kr3scw8ga=k31_$a
	TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTM1NTg1NzgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImltYXJ0aHVya2hlIn0.4v_y6TeV9zLEeO8Ks9coOFR8khTnCQNGUpvDKKgXotI

Чтобы убедиться в работе сервиса, необходимо добаить данные: теги, коды мобильных операторов, клиентов и создать рассылки. Это можно сделать в админке django.

	